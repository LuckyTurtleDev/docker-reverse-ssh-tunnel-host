FROM alpine

LABEL org.opencontainers.image.url="https://gitlab.com/Lukas1818/docker-reverse-ssh-tunnel-host/container_registry"
LABEL org.opencontainers.image.title="image to host a ssh server for a ssh reverse tunnel, with minimal configuration"
LABEL org.opencontainers.image.source="https://gitlab.com/Lukas1818/docker-reverse-ssh-tunnel-host"


Run apk update \
 && apk add \
      openssh \
      ca-certificates \
 && cp /etc/ssh/sshd_config /sshd_config

ADD entrypoint.sh /entrypoint.sh

RUN addgroup --gid 1000 -S user \
 && adduser -u 1000 -S user -G user \
 && chown user -R /etc/ssh/ \
 && chmod a+x /entrypoint.sh

USER user:user

CMD ["/entrypoint.sh"]
